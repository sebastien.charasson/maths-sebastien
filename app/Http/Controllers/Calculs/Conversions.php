<?php

namespace App\Http\Controllers\Calculs;

class Conversions
{
    public static function convertTemp($temperature, $uniteInitiale, $unite){
        if($uniteInitiale == $unite){
            $res = $temperature;
        }
        else if($uniteInitiale == "°C"){
            $res = $temperature *9/5 + 32;
        }
        else{
            $res = ($temperature - 32) * 5/9;
        }

        return $res;
    }


    public static function convertDistance($distance, $uniteInitiale, $unite){
        $res = 0;
            switch ($uniteInitiale){
                case "km":
                switch ($unite){
                    case "hm":
                        $res=$distance*10;
                        break;
                    case "dam":
                        $res=$distance*100;
                        break;
                    case "m":
                        $res=$distance*1000;
                        break;
                    case "dm":
                        $res=$distance*10000;
                        break;
                    case "cm":
                        $res=$distance*100000;
                        break;
                    case "mm":
                        $res=$distance*1000000;
                        break;
                    default:
                        $res=$distance;
                    }
                break;
                case "hm":
                    switch ($unite){
                        case "km":
                            $res=$distance/10;
                            break;
                        case "dam":
                            $res=$distance*10;
                            break;
                        case "m":
                            $res=$distance*100;
                            break;
                        case "dm":
                            $res=$distance*1000;
                            break;
                        case "cm":
                            $res=$distance*10000;
                            break;
                        case "mm":
                            $res=$distance*100000;
                            break;
                        default:
                            $res=$distance;
                        }
                    break;
                case "dam":
                    switch ($unite){
                        case "km":
                            $res=$distance/100;
                            break;
                        case "hm":
                            $res=$distance/10;
                            break;
                        case "m":
                            $res=$distance*10;
                            break;
                        case "dm":
                            $res=$distance*100;
                            break;
                        case "cm":
                            $res=$distance*1000;
                            break;
                        case "mm":
                            $res=$distance*10000;
                            break;
                        default:
                            $res=$distance;
                        }
                    break;
                case "m":
                    switch ($unite){
                        case "km":
                            $res=$distance/1000;
                            break;
                        case "hm":
                            $res=$distance/100;
                            break;
                        case "dam":
                            $res=$distance/10;
                            break;
                        case "dm":
                            $res=$distance*10;
                            break;
                        case "cm":
                            $res=$distance*100;
                            break;
                        case "mm":
                            $res=$distance*1000;
                            break;
                        default:
                            $res=$distance;
                        }
                    break;
                case "dm":
                    switch ($unite){
                        case "km":
                            $res=$distance/10000;
                            break;
                        case "hm":
                            $res=$distance/1000;
                            break;
                        case "dam":
                            $res=$distance/100;
                            break;
                        case "m":
                            $res=$distance/10;
                            break;
                        case "cm":
                            $res=$distance*10;
                            break;
                        case "mm":
                            $res=$distance*100;
                            break;
                        default:
                            $res=$distance;
                        }
                    break;
                case "cm":
                    switch ($unite){
                        case "km":
                            $res=$distance/100000;
                            break;
                        case "hm":
                            $res=$distance/10000;
                            break;
                        case "dam":
                            $res=$distance/1000;
                            break;
                        case "m":
                            $res=$distance/100;
                            break;
                        case "dm":
                            $res=$distance/10;
                            break;
                        case "mm":
                            $res=$distance*10;
                            break;
                        default:
                            $res=$distance;
                        }
                    break;
                case "mm":
                    switch ($unite){
                        case "km":
                            $res=$distance/1000000;
                            break;
                        case "hm":
                            $res=$distance/100000;
                            break;
                        case "dam":
                            $res=$distance/10000;
                            break;
                        case "m":
                            $res=$distance/1000;
                            break;
                        case "dm":
                            $res=$distance/100;
                            break;
                        case "cm":
                            $res=$distance/10;
                            break;
                        default:
                            $res=$distance;
                        }
                    break;
                default:
            }
        return $res;
    }

}
