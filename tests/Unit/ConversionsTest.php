<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Http\Controllers\Calculs\Conversions;

class ConversionsTest extends TestCase
{
    public function testConvertTempCToF()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $temperature = 5;
        $uniteInitiale = "°C";
        $unite = "°F";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertTemp($temperature, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 41;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertTempFToC()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $temperature = 5;
        $uniteInitiale = "°F";
        $unite = "°C";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertTemp($temperature, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = -15;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    public function testConvertTempFToF()
    {
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $temperature = 5;
        $uniteInitiale = "°F";
        $unite = "°F";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertTemp($temperature, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 5;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

    //Les TU Distance
    //KM
    public function testConvertDistanceKMtoKM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "km";
        $unite = "km";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceKMtoHM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "km";
        $unite = "hm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 20;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceKMtoDAM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "km";
        $unite = "dam";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 200;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceKMtoM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "km";
        $unite = "m";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 2000;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceKMtoDM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "km";
        $unite = "dm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 20000;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceKMtoCM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "km";
        $unite = "cm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 200000;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceKMtoMM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "km";
        $unite = "mm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 2000000;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    //HM
    public function testConvertDistanceHMtoKM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "hm";
        $unite = "km";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceHMtoHM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "hm";
        $unite = "hm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceHMtoDAM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "hm";
        $unite = "dam";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 20;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceHMtoM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "hm";
        $unite = "m";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 200;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceHMtoDM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "hm";
        $unite = "dm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 2000;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceHMtoCM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "hm";
        $unite = "cm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 20000;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceHMtoMM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "hm";
        $unite = "mm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 200000;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    //DAM
    public function testConvertDistanceDAMtoKM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dam";
        $unite = "km";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.02;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceDAMtoHM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dam";
        $unite = "hm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceDAMtoDAM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dam";
        $unite = "dam";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceDAMtoM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dam";
        $unite = "m";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 20;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceDAMtoDM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dam";
        $unite = "dm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 200;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceDAMtoCM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dam";
        $unite = "cm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 2000;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceDAMtoMM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dam";
        $unite = "mm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 20000;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    //M
    public function testConvertDistanceMtoKM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "m";
        $unite = "km";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.002;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceMtoHM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "m";
        $unite = "hm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.02;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceMtoDAM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "m";
        $unite = "dam";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceMtoM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "m";
        $unite = "m";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceMtoDM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "m";
        $unite = "dm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 20;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceMtoCM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "m";
        $unite = "cm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 200;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceMtoMM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "m";
        $unite = "mm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 2000;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    //DM
    public function testConvertDistanceDMtoKM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dm";
        $unite = "km";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.0002;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceDMtoHM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dm";
        $unite = "hm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.002;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceDMtoDAM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dm";
        $unite = "dam";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.02;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceDMtoM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dm";
        $unite = "m";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceDMtoDM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dm";
        $unite = "dm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceDMtoCM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dm";
        $unite = "cm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 20;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceDMtoMM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "dm";
        $unite = "mm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 200;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    //CM
    public function testConvertDistanceCMtoKM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "cm";
        $unite = "km";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.00002;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceCMtoHM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "cm";
        $unite = "hm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.0002;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceCMtoDAM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "cm";
        $unite = "dam";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.002;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceCMtoM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "cm";
        $unite = "m";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.02;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceCMtoDM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "cm";
        $unite = "dm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceCMtoCM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "cm";
        $unite = "cm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceCMtoMM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "cm";
        $unite = "mm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 20;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    //CM
    public function testConvertDistanceMMtoKM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "mm";
        $unite = "km";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.000002;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceMMtoHM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "mm";
        $unite = "hm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.00002;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceMMtoDAM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "mm";
        $unite = "dam";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.0002;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceMMtoM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "mm";
        $unite = "m";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.002;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceMMtoDM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "mm";
        $unite = "dm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.02;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceMMtoCM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "mm";
        $unite = "cm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 0.2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }
    public function testConvertDistanceMMtoMM(){
        //Je définis les paramètres nécessaires
        //pour appeler la méthode convertTemp
        $distance = 2;
        $uniteInitiale = "mm";
        $unite = "mm";

        //J'appelle la méthode convertTemp() et je stocke
        //la valeur de retour dans une variable
        $resultat = Conversions::convertDistance($distance, $uniteInitiale, $unite);
        //Je définis le résultat attendu
        $expected = 2;

        //Je vérifie que le résultat retourné par convertTemp()
        //correspond à ce qui est attendu
        $this->assertSame($expected, $resultat);
    }

}
